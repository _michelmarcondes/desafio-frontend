import React, { Component } from 'react';
import Moment from 'react-moment';
import LazyLoad from 'react-lazyload';
import FadeImage from 'react-fade-image';

export default class Comic extends Component {
    render() {
        return (
            <LazyLoad height={250} once offset={100} >
                <article className="comic-container">
                    <div className="comic-cover-container">
                        <FadeImage
                            ratio='9:16'
                            src={`${this.props.coverUrl}/portrait_xlarge.${this.props.coverExtension}`} 
                            loaderComponent={<div className='comic-loader'><img src='/assets/resource-loading.gif' alt='loading' /></div>}
                            className="comic-cover"
                            alt={this.props.title}
                        />
                    </div>
                    <div className="comic-text-container">
                        <h2 className="featured-title">{this.props.title}</h2>
                        <Moment format='DD/MM/YYYY'>{this.props.date}</Moment>
                        <p>{this.props.description}</p>
                    </div>
                </article>
            </LazyLoad>
        );
    }
}