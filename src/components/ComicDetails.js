import React, { Component } from 'react';
import $ from 'jquery';

export default class ComicDetails extends Component {
    componentDidMount() {
        let comicData = this.props.comicData;
        
        this.renderComicDetailTop(comicData);
        this.iterate(comicData, '');
    }

    renderComicDetailTop = (comic) => {
        $('#detailContainer')
            .append(
            `<div class='comic-detail-top'>
                <div class='comic-detail-headline'>${comic.title}</div>
                <div>
                    <img 
                        class='comic-detail-cover' 
                        src='${comic.thumbnail.path}/landscape_amazing.${comic.thumbnail.extension}' 
                        alt='${comic.title}' 
                    />
                </div>
            </div>`
            );
    }

    //get and render all properties dynamically
    iterate = (obj, stack) => {
        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {
                if (typeof obj[property] === "object") {
                    this.iterate(obj[property], stack + '.' + property);
                } else {
                    $('#detailContainer')
                        .append(
                        this.formatInfo(property, obj[property])
                        );
                }
            }
        }
    }

    formatInfo = (key, value) => {
        //is url
        if (key === 'url') {
            value = `<a href='${value}' target='_blank'>LINK</a>`;
        }

        //is image
        if (key === 'path') {
            value = `<img src='${value}/portrait_small.jpg' />`;
        }

        //default value
        if (!value || value === '' || value === undefined) {
            value = ' - ';
        }

        return `<div class='comic-detail-container'>
                    <div class='comic-detail-keys'>${key}</div> 
                    <div class='comic-detail-values'>${value}</div>
                </div>`;
    }

    render() {
        return (
            <div>
                <div id="detailContainer">
                </div>
                <div className="comic-detail-close-button" onClick={this.props.closeModal}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 36 36">
                        <path d="M28.5 9.62L26.38 7.5 18 15.88 9.62 7.5 7.5 9.62 15.88 18 7.5 26.38l2.12 2.12L18 20.12l8.38 8.38 2.12-2.12L20.12 18z"></path>
                    </svg>
                </div>
            </div>
        );
    }
}