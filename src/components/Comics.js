import React, { Component } from 'react';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroller';
import Modal from 'react-responsive-modal';

import Comic from './Comic';
import ComicDetails from './ComicDetails';

const ironManId = 1009368;
const API_KEY = 'ec731b4e6cd615c2b1a7266df12dcf8e';
const ROOT_URL = 'https://gateway.marvel.com:443/v1/public/characters';
const DEFAULT_LIMIT = 10;

export default class Comics extends Component {
    state = {
        // data: null,
        results: [],
        hasMoreItems: true,
        comicsOffset: 0,
        comicsTotal: 0,
        modalIsOpen: false,
        comicData: null
    };

    getComics = async (page) => {
        try {
            let { data } = await axios.get(`${ROOT_URL}/${ironManId}/comics`,
                {
                    headers: { 'Accept': '*/*' },
                    params: {
                        apikey: API_KEY,
                        ts: '1511796807392',
                        hash: '3a6279c7acc3cd78426149de1a162903',
                        format: 'comic',
                        formatType: 'comic',
                        orderBy: '-onsaleDate',
                        noVariants: true,
                        limit: DEFAULT_LIMIT,
                        offset: this.state.comicsOffset
                    }
                });

            if (data) {
                //adding new data on main resource
                let results = this.state.results;
                data.data.results.map((item) => {
                    results.push(item);
                    return true;
                });

                //setting up next API request offset
                let newOffset = this.state.comicsOffset + DEFAULT_LIMIT;
                this.setState({ comicsOffset: newOffset });

                //setting up comics total
                if (this.state.comicsTotal === 0) {
                    this.setState({ comicsTotal: data.data.total });
                }

                //stopping requests
                if (this.state.comicsOffset >= this.state.comicsTotal) {
                    this.setState({ hasMoreItems: false });
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    getDate = (dateObject) => {
        let date = null;

        if (dateObject) {
            dateObject.map(item => {
                if (item.date !== undefined && item.type === 'onsaleDate') {
                    date = item.date;
                }
                //best practice recommendation
                return true;
            });
        }

        return date;
    }

    openModal = (comicData) => {
        this.setState({ modalIsOpen: true, comicData });
    }

    closeModal = () => {
        this.setState({ modalIsOpen: false, comicData: null });
    }

    renderComics() {
        if (this.state.results) {
            return this.state.results.map((item, index) => {
                let comicDate = this.getDate(item.dates);

                if (comicDate && item.description) {
                    return <div key={index} onClick={() => this.openModal(item)}>
                        <Comic
                            coverUrl={item.thumbnail.path}
                            coverExtension={item.thumbnail.extension}
                            title={item.title}
                            date={comicDate}
                            description={item.description}
                        />
                    </div>
                }
                return null;
            });
        }
    }

    render() {
        return (
            <div>
                <InfiniteScroll
                    pageStart={0}
                    initialLoad
                    loadMore={this.getComics}
                    hasMore={this.state.hasMoreItems}
                    loader={<div className="loader">Loading ...</div>}
                >
                    {this.renderComics()}
                </InfiniteScroll>


                <Modal
                    open={this.state.modalIsOpen}
                    onClose={this.closeModal}
                    classNames={{
                        overlay: 'comic-detail-modal-overlay',
                        modal: 'comic-detail-modal'
                    }}
                >
                    <ComicDetails
                        comicData={this.state.comicData}
                        closeModal={this.closeModal}
                    />
                </Modal>
            </div>
        );
    }
}