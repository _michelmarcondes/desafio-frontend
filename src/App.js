import React, { Component } from 'react';

import './App.css';
import Comics from './components/Comics';

export default class App extends Component {
    render() {
        return (
            <div className="app">
            <header className="app-header">
              <img src="/assets/marvel.svg" className="app-logo" alt="logo" />
              <h2 className="page-title text-shadow">IRON MAN: Comics</h2>
            </header>
            <div className='container'>
              <section className='principal-panel'>
                <Comics />
              </section>
            </div>
            <footer className="app-footer">
              <h4>Data provided by Marvel. © 2017 Marvel</h4>
            </footer>
          </div>
        );
    }
}